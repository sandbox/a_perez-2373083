<?php
module_load_include('inc', 'blazegraph_api', 'includes/blazegraph_api.api');

function blazegraph_api_example_page(){
  $datasource = base_path().'blazegraph_api/example/xml';
  $divid      = 'exampleBlazegraphContainer';
  $width      = '100%';
  $height     = '400px';
  
  $output = blazegraph_api_draw_graph($datasource,$divid,$width,$height);
  return $output;
}

function blazegraph_api_example_xml(){
  $blazegraph = array();
  $blazegraph['nodeset'] = blazegraph_api_example_get_nodeset();
  $blazegraph['edgeset'] = blazegraph_api_example_get_edgeset();

  // This page must be an XML
  header ("Content-Type:text/xml");
  $page	=	blazegraph_api_get_xml($blazegraph);
  print ($page);
}

function blazegraph_api_example_get_nodeset(){
  $nodeset = array();
  for ($i = 1; $i <= 10; $i++) {
    $node = array();

    //Show just the first node by default, others will show with users interaction
    $visibility = "false";
    if ($i == 1){
      $visibility = "true";
    }

    $node['nodeid'] = $i;
    
    $node['location']['x']       = 0;
    $node['location']['y']       = 50 * ($i - 1);
    $node['location']['visible'] = $visibility;

    $node['label']['label']     = "Node $i";
    $node['label']['shape']     = 1;
    $node['label']['backcolor'] = "00db77";
    $node['label']['textcolor'] = "FFFFFF";
    $node['label']['fontsize']  = 14;
    
    // Add these fields if the node has an URL
    /*$node['url']['url']         = $url;
    $node['url']['urlIsLocal']  = "true";
    $node['url']['urlUsXML']    = "false";*/

    $nodeset[] = $node;
  }

function blazegraph_api_example_get_edgeset(){
  $edgeset = array();

  //Each node linked to the next one
  for ($i = 1; $i <= 10; $i++) {
    $edge = array();

    if ($i == 10){
      $toID = 1;
    }
    else{
      $toID = $i + 1;
    }

    $edge['fromID']  = $i;
    $edge['toID']    = $toID;
    $edge['type']    = 4;
    $edge['length']  = 25;
    $edge['visible'] = "false";
    $edge['color']   = "A0A0A0";

    $edgeset[] = $edge;
  }

  //Link first node with the 5th one
  $edge['fromID']  = 1;
  $edge['toID']    = 5;
  $edge['type']    = 4;
  $edge['length']  = 25;
  $edge['visible'] = "false";
  $edge['color']   = "A0A0A0";

  $edgeset[] = $edge;

  return $edgeset;
}

  return $nodeset;
}

