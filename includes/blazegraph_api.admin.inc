<?php
/**
 * Implementation of hook_admin().
 * Module configuration in "Administer -> Site Configuration -> blazegraph"
 */
function blazegraph_api_admin() {
  $form = array();
  $form['blazegraph_path'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Blazegraph folder path'),
    '#default_value' => variable_get('blazegraph_path',DEFAULT_BLAZEGRAPH_PATH),
    '#description'   => t('The path where the files blazegraph.swf, blazegraph.css, blazegraph.js and swfobject.js are. These files can be downloaded from <a href="@link">Blazegraph website</a>', array('@link' => BLAZEGRAPH_WEBSITE)),
    '#suffix'        => '<p>'.t('<a href="@link">Example page</a> to test configuration and se how Blazegraph works.', array('@link' => base_path().'blazegraph_api/example')).'<p>',
);
  return system_settings_form($form);
}

/**
 * Validate the form of hook_admin().
 */
function blazegraph_api_admin_validate($form, &$form_state) {
  if($form_state['values']['blazegraph_path'] == ''){
    form_set_error('blazegraph_path', t('Path can not be empty.'));
  }
}

