<?php
module_load_include('inc', 'blazegraph_api', 'includes/blazegraph_api.api');

function blazegraph_api_example_page(){
  $datasource = 'blazegraph/example/xml';
  $divid      = 'exampleBlazegraphContainer';
  
  $output = blazegraph_api_draw_graph($datasource,$divid);
  return $output;
}

function blazegraph_api_example_xml(){
  $blazegraph = array();
  $blazegraph['nodeset'] = blazegraph_api_example_get_nodeset();
  $blazegraph['edgeset'] = blazegraph_api_example_get_edgeset();

  // This page must be an XML
  header ("Content-Type:text/xml");
  $page	=	blazegraph_api_get_xml($blazegraph);
  print ($page);
}

function blazegraph_api_example_get_edgeset(){
  $edgeset = array();

  //Each node linked to the next one
  for ($i = 1; $i <= 10; $i++) {
    $edge = array();

    if ($i == 10){
      $toID = 1;
    }
    else{
      $toID = $i + 1;
    }

    $edge['fromID']  = $i;
    $edge['toID']    = $toID;
    $edge['type']    = 4;
    $edge['length']  = 25;
    $edge['visible'] = "true";
    $edge['color']   = "A0A0A0";

    $edgeset[] = $edge;
  }

  return $edgeset;
}
function blazegraph_api_example_get_nodeset(){
  $nodeset = array();
  for ($i = 1; $i <= 10; $i++) {
    $node = array();

    $node['nodeid'] = $nodeid;
    
    $node['location']['x']       = 100;
    $node['location']['y']       = 50*$i;
    $node['location']['visible'] = "true";

    $node['label']['label']     = "Node $i";
    $node['label']['shape']     = 1;
    $node['label']['backcolor'] = "00db77";
    $node['label']['textcolor'] = "FFFFFF";
    $node['label']['fontsize']  = 14;
    
    // Add these fields if the node has an URL
    /*$node['url']['url']         = $url;
    $node['url']['urlIsLocal']  = "true";
    $node['url']['urlUsXML']    = "false";*/

    $node['hint']['hint']   = "This is a hint for Node $i";
    $node['hint']['width']  = 300;
    $node['hint']['height'] = 300;
    $node['hint']['isHTML'] = false;

    $nodeset[] = $node;
  }

  return $nodeset;
}

