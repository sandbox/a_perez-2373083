<?php
/**
 * Function to create a page with some help for the users
 * 
 * @return
 * 	String with the html code to display the help
 */
function blazegraph_api_userhelp(){
  //$page	=  '<h3>'.t('User help')."</h3>";
  $page	.= '<br/>
    <h3>'. t("You can change some parameters changing the option in the menu and moving the scroll bar."). '</h3>
    <dl>
      <dt>Zoom</dt>
      <dd>'. t("Makes the <b>objects size</b> bigger or smoller.") .'</dd>
      <dt>Spacing</dt>
      <dd>'. t("Makes the <b>space between nodes</b> bigger or smoller.") .'</dd>
      <dt>Rotate</dt>
      <dd>'. t("Rotates all the objects.").'</dd>
      <dt>Locality</dt>
      <dd>'. t("Having the selected node as reference, will <b>expand or contract the other nodes relations</b>.") .'</dd>
    </dl><br/>
    <h3>'. t("You can <b>stop the animation</b> clicking in the 'pause' checkbox in the upper side. Very recomendable to stop animation with many objects in the screen."). '</h3><br/>
    <h3>'.t("<b>Right clicking</b> with the mouse will display a menu with different options"). '</h3>
    <dl>
      <dt>Select Node</dt>
      <dd>'. t("It will select the clicked node.") .'</dd>
      <dt>Hide node</dt>
      <dd>'.t("It will shrink all nodes but the clicked one.").'</dd>
      <dt>Expand Node</dt>
      <dd>'.t("It will show all nodes related to the clicked one.").'</dd>
      <dt>Open node URL</dt>
      <dd>'.t("It will open the nodes URL in a <b>new tab/window</b>.").'</dd>
    </dl><br/>';

  return $page;
}
