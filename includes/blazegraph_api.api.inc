<?php
/**
 * Adds needed JavaScript and CSS files. 
 * Then returns the needed HTML code to write the graph.
 * Enter description here ...
 * @param String $datasource String with the URL where the data XML can be found
 * @param String $divid  String with the ID of the HTML element will contain the graph.
 */
function blazegraph_api_draw_graph($datasource,$divid="blazegraphContainer",$width='100%',$height='500px'){
  $path   = variable_get('blazegraph_path',DEFAULT_BLAZEGRAPH_PATH);
  $css    = "$path/blazegraph.css";
  $js     = "$path/blazegraph.js";
  $js_swf = "$path/swfobject.js";
 
  drupal_add_css($css);
  drupal_add_js($js);
  drupal_add_js($js_swf);
 
  $datasource = $datasource;
  $swf = base_path()."$path/blazegraph.swf";
  $code = "
    function drawBlazegraph() {
      var so = new SWFObject('$swf', 'blazegraph', '$width', '$height', '8', '#FFFFFF');
      so.addVariable('id', '$divid');
      so.addVariable('datasource', '$datasource');
      so.addParam('wmode', 'transparent');
      so.write('$divid');
     }";
  drupal_add_js($code,'inline');

  $page = l('help', "blazegraph_api/userhelp") . " | <a href='$datasource'>xml</a>
    <br/>
    <div id='$divid'></div>
    <SCRIPT LANGUAGE='JavaScript' TYPE='TEXT/JAVASCRIPT'>
      drawBlazegraph()
    </SCRIPT>";

  return $page;
}

/**
 * Function to create the xml with the data info
 * @param $blazegraph An array with the nodeset, edgeset and parameters
 * @return string An String with the XML code of the data
 */
function blazegraph_api_get_xml($blazegraph){
  $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>
    <TOUCHGRAPH_LB version="1.20">';
  $xml .= blazegraph_api_prepare_nodeset($blazegraph['nodeset']);
  $xml .= blazegraph_api_prepare_edgeset($blazegraph['edgeset']);
  $xml .= blazegraph_api_prepare_parameters($blazegraph['parameters']);
  $xml .= '</TOUCHGRAPH_LB>';
  return $xml;
}

/**
 * Creates Nodeset XML part.
 * @param Array $nodeset Data from the nodeset.
 * @return string An string with the nodeset part of the XML.
 */
function blazegraph_api_prepare_nodeset($nodeset){
  $xml = "<NODESET>";
  if(count($nodeset))
    foreach($nodeset as $node){
      $nodeid = isset($node['nodeid']) ? $node['nodeid'] : "";
   
      $x       = isset($node['location']['x'])       ? $node['location']['x']       : "0";
      $y       = isset($node['location']['y'])       ? $node['location']['y']       : "0";
      $visible = isset($node['location']['visible']) ? $node['location']['visible'] : "true";

      $xml .= "<NODE nodeID='$nodeid'>
                <NODE_LOCATION 
                 x='$x'
                 y='$y' 
                 visible='$visible'/>";
   
      $label     = isset($node['label']['label'])     ? $node['label']['label']     : "";
      $shape     = isset($node['label']['shape'])     ? $node['label']['shape']     : 2;
      $backcolor = isset($node['label']['backcolor']) ? $node['label']['backcolor'] : "0058db";
      $textcolor = isset($node['label']['textcolor']) ? $node['label']['textcolor'] : "FFFFFF";
      $fontsize  = isset($node['label']['fontsize'])  ? $node['label']['fontsize']  : 16;

      $xml .= "
                <NODE_LABEL 
                 label='".blazegraph_api_prepare_str($label)."' 
                 shape='$shape' 
                 backColor='$backcolor' 
                 textColor='$textcolor' 
                 fontSize='$fontsize'
                />";

      if (isset($node['url'])){
        $url        = isset($node['url']['url'])        ? $node['url']['url']        : "";
        $urlIsLocal = isset($node['url']['urlIsLocal']) ? $node['url']['urlIsLocal'] : "true";
        $urlIsXML   = isset($node['url']['urlIsXML'])   ? $node['url']['urlIsXML']   : "false";
        $xml .= "
                <NODE_URL 
                 url='$url' 
                 urlIsLocal='$urlIsLocal'
                 urlIsXML='$urlIsXML'/>";
      }
   
      if (isset($node['hint'])){
        $hint   = isset($node['hint']['hint'])   ? $node['hint']['hint']   : "";
        $width  = isset($node['hint']['width'])  ? $node['hint']['width']  : 300;
        $height = isset($node['hint']['height']) ? $node['hint']['height'] : 150;
        $isHTML = isset($node['hint']['isHTML']) ? $node['hint']['isHTML'] : "true";
   
      
                
        $xml .= "
                <NODE_HINT 
                 hint='".blazegraph_api_prepare_hint($hint)."'
                 width='$width'
                 height='$height'
                 isHTML='$isHTML'/>";
      }

      $xml .= "</NODE>";
  }
  $xml .= "</NODESET>";

  return $xml;
}


/**
 * Creates EDGESET XML part.
 * @param $edgeset the Edgeset.
 * @return string An string with the XML of the edgeset.
 */
function blazegraph_api_prepare_edgeset($edgeset){
  $xml = "<EDGESET>";
  if(count($edgeset))
    foreach($edgeset as $edge){
      $fromID  = isset($edge['fromID'])  ? $edge['fromID']  : "";
      $toID    = isset($edge['toID'])    ? $edge['toID']    : "";
      $type    = isset($edge['type'])    ? $edge['type']    : 4;
      $length  = isset($edge['length'])  ? $edge['length']  : 750;
      $visible = isset($edge['visible']) ? $edge['visible'] : "false";
      $color   = isset($edge['color'])   ? $edge['color']   : "A0A0A0";
   
      $xml .= "<EDGE
        fromID='$fromID'
        toID='$toID'
        type='$type'
        length='$length'
        visible='$visible'
        color='$color'
        />";
  }
  $xml .= "</EDGESET>";

  return $xml;
}

/**
 * Creates parameters XML part.
 * @param $param the parameter.
 * @return string An string with the XML of the parameter.
 */
function blazegraph_api_prepare_parameters($param){
  $offsetX  = isset($param['offsetX'])  ? $param['offsetX']  : 0;
  $rotateSB = isset($param['rotateSB']) ? $param['rotateSB'] : 0;
  $zoomSB   = isset($param['zoomSB'])   ? $param['zoomSB']   : -7;
  $offsetY  = isset($param['offsetY'])  ? $param['offsetY']  : 0;
 
  $xml = 
  "<PARAMETERS>
     <PARAM name='offsetX'  value='$offsetX'/>
     <PARAM name='rotateSB' value='$rotateSB'/>
     <PARAM name='zoomSB'  value='$zoomSB'/>
     <PARAM name='offsetY'  value='$offsetY'/>
   </PARAMETERS>";

  return $xml;
}
/**
 * Encodes string in UTF-8, capitalizes the first letter of the string and lowercases the others.
 * @param $str
 *  Input sentence/word to change
 *
 * @return
 *  A string with the result (UCF-8 encoding, firs letter capitalized and lowercased the others)
 */
function blazegraph_api_prepare_str($str){
 //Uppercase the first letter and the other
 return ucfirst(
  strtolower(
   utf8_decode($str)
  )
 );
}
/**
 * Change '<' characters to '&lt;', '>' to '&gt;' and '"' to '&quot;'.
 * @param $hint
 *  The string to change
 * 
 * @return
 *  Changed string
 */
function blazegraph_api_prepare_hint($hint){
 $hint = str_replace("<",'&lt;',$hint);
 $hint = str_replace(">",'&gt;',$hint);
 $hint = str_replace('"','&quot;',$hint);
 $hint = str_replace("'",'&quot;',$hint);
 //$hint = str_replace("-",'&ndash;',$hint);
 
 return $hint;
}
