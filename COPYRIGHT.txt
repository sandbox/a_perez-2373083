All Blazegraph API module code is copyright 2010 - 2014 by Mondragon Goi  Eskola 
Politeknikoa JMA S.Coop., Alain Perez and Lorea Lorenzo.

This module is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with this program as the file LICENSE.txt; if not, please see
http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt.

Blazegraph module includes works under other copyright notices and distributed
according to the terms of the GNU General Public License or a compatible
license, including:

	Drupal 		- Copyright (c) 2001 - 2010 by the original authors.
	jQuery 		- Copyright (c) 2008 - 2009 John Resig

This module is suposed to be used with Blazegraph Flash aplication created 
by Max M. Petrov under Apache (v2) licence. (http://sourceforge.net/projects/blazegraph/)
