#------------------------------------------------------
#  Blazegraph API
#------------------------------------------------------
#  Blazegraph is a dynamic graph layout engine similar to TouchGraph link browser (tglinkbrowser) implemented in Flash and ActionScript. This module creates an API in order to enable it's use to other modules.
#
#  INSTALLING
#
#    (1) Copy the module folder into sites/all/modules/contrib directory
#    (2) Download blazegraph from http://sourceforge.net/projects/blazegraph/
#    (2) Unzip and copy the folder to sites/all/libraries
#    (3) If you put those files in another folder, go to "Administer -> Site Configuration -> blazegraph" and change the path.

